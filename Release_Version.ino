//Release Version 4/18/18

//program fully functional
//for liquid juice dispensors call to D10, D11, D12, D13 but are physically located at D3, D5, D7, D9. thats where the cup will go to fill when called D10 through D13

//edit out Serial.Begin(9600) to run without debug console being open
//command input format (number of ingredients, station of ingredient 1, ounces of ingredient 1, staion of ingredient 2,....)
//example: 2, 3, 1, 5, 1 this goes to station 3 pours 1 ounce then goes to station 5 pours 1 ounce

//spare matrix location 14 and 15. to turn on Spare15 write PORTA=123; for Spare14 write PORTA=125;

void LED(int color);                         //control led strip lights green, blue and red
void motorGoToDispenser(int station);       //insert dispenser number into function, returns when position is reached
void cupPosition(void);                     //function reads and updates encoder value
void motorGoToPos(int location);            //function for motor driver
void motorReturn(void);                     //returns to start position and sets position to zero
void motorDropOff(void);                    //carries cup to end for pick up
void dispenser(int station, float Oz);    //function takes dispenser number and desired valve on time in milliseconds, returns when dispensed

    //rotary encoder
 int pinA = 3;  // Connected to CLK on KY-040   blue wire
 int pinB = 4;  // Connected to DT on KY-040    yellow wire
 int encoderPosCount = 0; 
 int pinALast;  
 int aVal;
 boolean bCW;

      //dispensors
 //arduino to relay pinout
//port# pin#--relay#
//PA0    22     I8      
//PA1    23     I7      
//PA2    24     I6      
//PA3    25     I5      
//PA4    26     I4      
//PA5    27     I3      
//PA6    28     I2     
//PA7    29     I1      

void setup() {  
    Serial.begin(9600);
  
  //dispensors
  DDRA=0xFF; //configure port A as output. logic 0 energizes relay
  PORTA=255; //pull up to keep relays off
  
  //motor output                                  green motor wire goes to center pole relay 1
  DDRB=255; //configure port B as output          brown wire center pole of relay 2
                                                 //white strip power wire to norm closed relay 1, jump to norm closed relay 2
  //rotary encoder
   pinMode (pinA,INPUT);                          //relay control, pin 52 to I1 (relay 1) and pin 53 to I2 (relay 2)
   pinMode (pinB,INPUT);
   pinALast = digitalRead(pinA);   
   

  //home switch to reset encoder                        momentary switch, green wire to pin 37 and black to ground
  DDRC=0;     //configure port C as input               
  PORTC=0xFF; //turn on pullup resistors
 
  motorReturn(); //bring motor to home position   

  DDRL=0xFF; //configure Port L as output
  PORTL=0; //pull up to keep relays off
  LED(1);   //turn on idle blue led
  int L=0;  //timer to switch led back to idle
}
int L=0;  //timer to switch led back to idle
void loop() {
  

  
   while (Serial.available() > 0) {
    
    L=0; //reset led idle timer when drink request is made
     
     float lineup[15]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        
        int N = Serial.parseInt();

           Serial.print(N);Serial.println(" ingredients");

        N=N*2;
    
            for(int i=0;i<N;i=i+2){        
              lineup[i]=Serial.parseInt();
              lineup[i+1]=Serial.parseInt();
             }  
 
            for(int i=0;i<N;i=i+2){
              motorGoToDispenser(lineup[i]);
              dispenser(lineup[i],lineup[i+1]);
              Serial.print("motor goes to station ");Serial.println(lineup[i]);
              Serial.print("dispensing ");Serial.print(lineup[i+1]);Serial.println(" ounces");
             
             }
      Serial.println(" ");
      N=0;
      motorReturn();      //drink complete take cup back to user

  }
  
  delay(200);
  Serial.println(L);
  L++;
  if(L==10){   // user has 8 seconds to remove cup, system goes back to idle state blue led
    LED(1);  //switch led back to idle
    Serial.println("idele ");
    L=0;
  }
      
}
void motorGoToDispenser(int station){

  switch(station){
    
    case 1:
    motorGoToPos(6);    //numbers inside function are the encoder ticks measured from start postion to directly under dispenser listed in case
    break;                //for this case, the motor comes on, when the encoder reads 6 ticks the motor turns off

    case 2:
    motorGoToPos(25);
    break;

    case 3:
    motorGoToPos(50);
    break;

    case 4:
    motorGoToPos(76);
    break;

    case 5:
    motorGoToPos(100);
    break;

    case 6:
    motorGoToPos(123);
    break;

    case 7:
    motorGoToPos(150);
    break;

    case 8:
    motorGoToPos(170);
    break;

    case 9:
    motorGoToPos(195);
    break; 

    case 10:
    motorGoToPos(50); 
    break;

    case 11:
    motorGoToPos(100);
    break;
    
    case 12:
    motorGoToPos(150);
    break;

    case 13:
    motorGoToPos(195);
    break;
    
  }

} //end motorGoToDispenser

void motorGoToPos(int location){      //this function determines if motor needs to go forward or backward to reach destination

LED(2); // cup moving turn on green led

  if(encoderPosCount<location){
      
      while(encoderPosCount<location){      //motor forward
        cupPosition();
        PORTB=1;
        }
       
  }

  else if(encoderPosCount>location){      //motor backward

      while(encoderPosCount>location){
        cupPosition();
        PORTB =2;
      }      
  }
  
PORTB=3;        //motor stop

} //end motorGoToPos 

void motorReturn(void){     // function returns motor to home postion and resets encoder

LED(4); //drink complete turn on blue and red led

  while(PINC==255){
    cupPosition();
    PORTB=2;
    //Serial.println(PINC);
    delay(10);
  }
   PORTB=3;
   Serial.println("off");
   encoderPosCount=0;
   delay(100);
  
} //end motorReturn

void motorDropOff(void){      //this function carries the cup to the end of the deck which is 203 encoder clicks from start to end

  while(encoderPosCount<201){
    cupPosition();
    PORTB=1;
  }
  
  PORTB=3;
  delay(5000);
 
} //end motorDropOff

void cupPosition(void){                         //keeps up with rotary encoder postion
                                                 
   aVal = digitalRead(pinA);      
   
   if (aVal != pinALast){ 
  
       if (digitalRead(pinB) != aVal) {  // Means pin A Changed first - We're Rotating Clockwise
          encoderPosCount ++;
          bCW = true;
         } 
       else {// Otherwise B changed first and we're moving CCW
          bCW = false;
          encoderPosCount--;
         }  
     Serial.print("Postion  ");
     Serial.println(encoderPosCount);  
    
   } 
   
   pinALast = aVal;
   
} //end cupPosition 
 
//juice pump flow rate is 0.3422 ounces per second
 void dispenser(int station, float Oz){      //main function            this function turns on the dispenser of choice for timeon amount of time

      LED(3); //dispensing turn on green and red led
      
      float timeOn=0;
      float PumpOn=0;
     
     timeOn=Oz*1000;         //flow rate constant to determin time on with respect to ounces needed     
     PumpOn=Oz/0.3442;       //flow rate for juice pumps
     PumpOn=PumpOn*1000;        
  
 
  switch(station){               //relay #1 is driven by I8. relays 1,2,3 provide power to matrix. remainder relays provide ground.
                                //when I8 is low 0v relay 1 gets energized                      
    case 1:                    
    PORTA=246;            //solenoids on PortA
    PORTB=4;              //airlock airpump on PortB              
    delay(timeOn);
    break;

    case 2:
    PORTA=238;
    PORTB=4;      
    delay(timeOn);
    break;

    case 3:
    PORTA=222; 
    PORTB=4;     
    delay(timeOn);
    break;

    case 4:
    PORTA=245;
    PORTB=4;       
    delay(timeOn);
    break;

    case 5:
    PORTA=237;
    PORTB=4;       
    delay(timeOn);
    break;

    case 6:
    PORTA=221; 
    PORTB=4;      
    delay(timeOn);
    break;

    case 7:
    PORTA=243;
    PORTB=4;       
    delay(timeOn);
    break;

    case 8:
    PORTA=235;
    PORTB=4;         
    delay(timeOn);
    break;

    case 9:
    PORTA=219; 
    PORTB=4;      
    delay(timeOn);
    break;

    case 10:  //dispensor relocated to D3
    PORTA=187;   
    delay(PumpOn);
    break;

    case 11:   //dispensor relocated to D5
    PORTA=126;     
    delay(PumpOn);
    break;

    case 12:  //dispensor relocated to D7
    PORTA=189;     
    delay(PumpOn);
    break;

    case 13:   //dispensor relocated to D9
    PORTA=190;      
    delay(PumpOn);
    break;    
  }
  PORTB=0;
  PORTA=255;
  
  delay(1000); //drip time
} //end dispenser

void LED(int color){
  
    switch(color){
    
        case 1: // blue for idle
        PORTL=0;//255;
        break;
    
        case 2: //green for moving
        PORTL=1;//254;
        break;

        case 3: //green and red for dispensing
        PORTL=3;//252;
        break;

        case 4: //done blue red plus delay time
        PORTL=2;//253;
        break;      
  }
}



